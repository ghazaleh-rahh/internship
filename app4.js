//4
class Person {
   constructor(name1,name2){
       this.name1=name1;
       this.name2=name2;
   }
   addString(){
       return this.name1+ " " + this.name2;
   }
   upperCase(){
       return this.name1.toUpperCase() + " " + this.name2.toUpperCase();
   }
   capitalize(){
       return this.name1.charAt(0).toUpperCase() + this.name1.slice(1) + " "+
       this.name2.charAt(0).toUpperCase() + this.name2.slice(1);
   }
   print(){
       return document.getElementById('output').innerHTML = this.name1 + " " + this.name2;
   }
}

const obj = new Person ('alireza','mohamad');


console.log(obj.addString());

console.log(obj.upperCase());

console.log(obj.capitalize());

console.log(obj.print());



